#+TITLE: Zagyarakushi | zagyarakushi.gitlab.io
#+OPTIONS: title:nil
#+META_TYPE: website
#+DESCRIPTION: Zagyarakushi's website

* Welcome!
:PROPERTIES:
:CUSTOM_ID: Welcome
:END:

This website is dedicated to help people ascend to higher level and become woke. Here, you can become a modern renaissance like person.
Also don't forget to be l33t h4x0r, minimalist and A E S T H E T I C A L.

Check out the YouTube channel Zagyarakushi on [[https://invidious.io/][Invidious]] for more chadness.

* Posts
:PROPERTIES:
:CUSTOM_ID: Posts
:END:

#+INCLUDE: posts/blog.org :lines "7-12"
[[file:posts/blog.org][See more...]]

* Projects
:PROPERTIES:
:CUSTOM_ID: Projects
:END:

#+INCLUDE: projects/projects.org :lines "7-12"
[[file:projects/projects.org][See more...]]

* Past Research
:PROPERTIES:
:CUSTOM_ID: Past-Research
:END:

#+INCLUDE: past-research/past-research.org :lines "7-12"
[[file:past-research/past-research.org][See more...]]
